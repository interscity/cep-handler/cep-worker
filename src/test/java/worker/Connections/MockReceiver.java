package worker.Connections;

public class MockReceiver implements Receiver {

    private int Events;
    private long Time;

    public MockReceiver(int eventsPerPeriod,long Time){
        this.Events = eventsPerPeriod;
        this.Time = Time;
    }

    public void setTime(long time){
        this.Time = time;
    }

    public void setEvents(int Events){
        this.Events = Events;
    }


    @Override
    public void close(String argument) {

    }

    @Override
    public int eventsPerPeriod() {
        return Events;
    }

    @Override
    public long timeElapsed() {
        return Time;
    }
}
