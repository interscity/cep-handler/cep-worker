package worker.Connections;

import org.apache.avro.generic.GenericData;

public class MockSender implements Sender{

    private int eventsLost;

    public void setEventsLost(int i){
        this.eventsLost = i;
    }

    @Override
    public void stopSending() {

    }

    @Override
    public void restartSending() {

    }

    @Override
    public int getEventsLost() {
        return eventsLost;
    }

    @Override
    public void close(String type) {

    }

    @Override
    public void Publish(String routingKey, GenericData.Record event) {

    }
}
