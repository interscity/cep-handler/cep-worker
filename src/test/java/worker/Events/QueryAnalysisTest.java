package worker.Events;

import junit.framework.TestCase;


import java.util.HashMap;


public class QueryAnalysisTest extends TestCase {

    int cl = 2172;
    int c = 31006;
    String corredor = "Pirituba";
    String query2 = "SELECT o.p as tp, b.p as op, o.cl as cl,o.py as py, o.px as px,o.v as v, o.ta as ta, o.timestamp as timestamp from veli" + cl + "(p != " + c + ")#ext_timed(ta,30) as o, veli" + cl + "(p = " + c + ")#ext_timed(ta,30) as b WHERE distance(o.py, o.px, b.py, b.px) < 0.5 AND distance(o.py, o.px, b.py, b.px) > 0";
    String query3 = "SELECT cl, p, py, px, distance(py, px, prev(py), prev(px)) as d, distance(py, px, prev(py), prev(px))*1000*60*60/Math.abs(ta - prev(ta)) as v, ta, timestamp FROM bus" + cl + "#length(2) group by p ";
    String query4 = "SELECT cl, p, py, px, d, v, ta, timestamp FROM velf" + cl + " WHERE v > 0 AND v < 100";
    String query5 = "SELECT cl,p, py, px,ta,  avg(v) as speed, timestamp FROM veli" + cl + "#ext_timed(timestamp,300) group by p";
    String query6 = "Select cl, p, py, px,speed , avg(speed) as velocity, ta, timestamp FROM speedbus"+corredor+"#ext_timed(timestamp,300) HAVING speed < 10 and avg(speed) < 25";

    public void testGetStateType(){
        String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("length", QueryAnalysis.getStateType(query));
    }

    public void testGetStateType2(){
        assertEquals("ext_timed", QueryAnalysis.getStateType(query2));
    }

    public void testGetStateType3(){
        assertEquals("length", QueryAnalysis.getStateType(query3));
    }

    public void testGetStateType5(){
        assertEquals("ext_timed", QueryAnalysis.getStateType(query5));
    }

    public void testGetStateType6(){
        assertEquals("ext_timed", QueryAnalysis.getStateType(query6));
    }

    public void testGetStateNumber(){
        String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("4",QueryAnalysis.getStateNumber(query));
    }

    public void testGetStateNumber2(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("ta,30",QueryAnalysis.getStateNumber(query2));
    }
    public void testGetStateNumber3(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("2",QueryAnalysis.getStateNumber(query3));
    }

    public void testGetStateNumber5(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("timestamp,300",QueryAnalysis.getStateNumber(query5));
    }

    public void testGetStateNumber6(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        assertEquals("timestamp,300",QueryAnalysis.getStateNumber(query6));
    }

    public void testAnalyzeLength(){
        HashMap<String,Integer> counters = new HashMap<>();
        String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query);
        String stateNumber = QueryAnalysis.getStateNumber(query);
        Integer count = QueryAnalysis.analyzeQueryLengh(stateType,stateNumber);
        //assertTrue(counters.containsKey("123"));
        assertTrue(count.equals(4));
    }

    public void testAnalyzeLength3(){
        //HashMap<String,Integer> counters = new HashMap<>();
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query3);
        String stateNumber = QueryAnalysis.getStateNumber(query3);
        Integer count = QueryAnalysis.analyzeQueryLengh(stateType,stateNumber);
        //assertTrue(counters.containsKey("123"));
        assertTrue(count.equals(2));
    }

    public void testAnalyzeTime2(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query2);
        String stateNumber = QueryAnalysis.getStateNumber(query2);
        long count = QueryAnalysis.analyzeQueryTime(stateType,stateNumber);
        //in microsseconds
        assertEquals(30000L, count);
    }

    public void testAnalyzeTime(){
        String query = "SELECT average from MyAvroEvent#ext_timed(timestamp,4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query);
        String stateNumber = QueryAnalysis.getStateNumber(query);
        System.out.println("Stype : "+stateType+"\n Snumber : "+stateNumber);
        assertEquals(4000,QueryAnalysis.analyzeQueryTime(stateType,stateNumber));
    }

    public void testAnalyzeTime5(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query5);
        String stateNumber = QueryAnalysis.getStateNumber(query5);
        long count = QueryAnalysis.analyzeQueryTime(stateType,stateNumber);
        //in microsseconds
        assertEquals(300000L, count);
    }

    public void testAnalyzeTime6(){
        //String query = "SELECT average from MyAvroEvent#length(4)#uni(carId)";
        String stateType = QueryAnalysis.getStateType(query6);
        String stateNumber = QueryAnalysis.getStateNumber(query6);
        long count = QueryAnalysis.analyzeQueryTime(stateType,stateNumber);
        //in microsseconds
        assertEquals(300000L, count);
    }

    public void testHasDataWindow(){
        String query = "SELECT average from MyAvroEvent#ext_timed(timestamp,4)#uni(carId)";
        String query2 = "SELECT carType,carId FROM MyAvroEvent as Location WHERE carId = 7 ";
        assertTrue(QueryAnalysis.hasDataWindow(query));
        assertFalse(QueryAnalysis.hasDataWindow(query2));
    }

    public void testHasDataWindow2(){
        assertTrue(QueryAnalysis.hasDataWindow(query2));
    }

    public void testHasDataWindow3(){
        assertTrue(QueryAnalysis.hasDataWindow(query3));
    }

    public void testHasDataWindow4(){
        assertFalse(QueryAnalysis.hasDataWindow(query4));
    }

    public void testHasDataWindow5(){
        assertTrue(QueryAnalysis.hasDataWindow(query5));
    }

    public void testHasDataWindow6(){
        assertTrue(QueryAnalysis.hasDataWindow(query6));
    }


}
