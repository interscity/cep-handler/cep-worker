package worker.LoadBalancers;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import junit.framework.TestCase;
import redis.embedded.RedisServer;
import worker.Connections.MockReceiver;
import worker.Connections.RabbitmqReceiver;
import worker.Connections.Receiver;
import worker.DatabaseAccess.Lettuce;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class StateUsageTimeBasedTest extends TestCase {

    int cl = 2172;
    int c = 31006;
    String corredor = "Pirituba";
    String query2 = "SELECT o.p as tp, b.p as op, o.cl as cl,o.py as py, o.px as px,o.v as v, o.ta as ta, o.timestamp as timestamp from veli" + cl + "(p != " + c + ")#ext_timed(ta,30) as o, veli" + cl + "(p = " + c + ")#ext_timed(ta,30) as b WHERE distance(o.py, o.px, b.py, b.px) < 0.5 AND distance(o.py, o.px, b.py, b.px) > 0";
    String query3 = "SELECT cl, p, py, px, distance(py, px, prev(py), prev(px)) as d, distance(py, px, prev(py), prev(px))*1000*60*60/Math.abs(ta - prev(ta)) as v, ta, timestamp FROM bus" + cl + "#length(2) group by p ";
    String query4 = "SELECT cl, p, py, px, d, v, ta, timestamp FROM velf" + cl + " WHERE v > 0 AND v < 100";
    String query5 = "SELECT cl,p, py, px,ta,  avg(v) as speed, timestamp FROM veli" + cl + "#ext_timed(timestamp,300) group by p";
    String query6 = "Select cl, p, py, px,speed , avg(speed) as velocity, ta, timestamp FROM speedbus"+corredor+"#ext_timed(timestamp,300) HAVING speed < 10 and avg(speed) < 25";


    public void testfindEventTypetoRellocate() throws IOException {

        RedisServer redisServer = new RedisServer();
        redisServer.start();

        RedisClient redisClient = RedisClient.create("redis://localhost:6379/");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String,String> syncCommands = connection.sync();
        syncCommands.sadd("111:Inputs","001","002","003");
        syncCommands.set("111:Definition",query4);// no data window
        syncCommands.sadd("222:Inputs","001","002","004");
        syncCommands.set("222:Definition",query3);// lengh 2 data window
        syncCommands.sadd("333:Inputs","001","004");
        syncCommands.set("333:Definition",query5); // time window 300000
        connection.close();
        redisClient.shutdown();

        StateUsageTimeBased SUTB = new StateUsageTimeBased(true);

        Map<String, Receiver> receivers = new HashMap<>();

        receivers.put("001",new MockReceiver(100,10000));
        receivers.put("002",new MockReceiver(100000,100000));
        receivers.put("003",new MockReceiver(1000,1000));
        receivers.put("004",new MockReceiver(10,100000));


        System.out.println("open lettuce");

        Lettuce store = new Lettuce("localhost");
        System.out.println(store.getEventTypeDefinition("111"));
        System.out.println("001 epp "+receivers.get("001").eventsPerPeriod()+"  time "+receivers.get("001").timeElapsed());
        System.out.println(" equals "+receivers.get("001").timeElapsed()/receivers.get("001").eventsPerPeriod()+" time rank");
        System.out.println("004 epp "+receivers.get("004").eventsPerPeriod()+"  time "+receivers.get("004").timeElapsed());
        System.out.println(" equals "+receivers.get("004").timeElapsed()/receivers.get("004").eventsPerPeriod()+" time rank");

        System.out.println("add 111 rank");
        SUTB.addEventTypeToRank("111",receivers,store);
        System.out.println("add 222 rank");
        SUTB.addEventTypeToRank("222",receivers,store);
        System.out.println("add 333 rank");
        SUTB.addEventTypeToRank("333",receivers,store);
        System.out.println("added rank");


        // 111 - no data window, first to realocate
        // 222 - time window - 30000 microssecons - 30 seconds
        // 333 - lenght window=2 ,
        // 333 flow:
        // 001 -  (10000 timeelapsed/100 evets per period) 2 lenght  => 200 microsseconds
        // 004 -  (100000 timeelapsed/100 evets per period) 2 lenght  => 2000 microsseconds
        // max time 001||004 => 004 -> 2 seconds

        System.out.println("first assert");

        assertEquals("111",SUTB.findEventTypetoRellocate());

        /*for(ScoredStringL al : SUTB.TimeRanked){
            System.out.println(al.getString()+" , "+al.getScore());
        }*/

        System.out.println("deletint 111");

        SUTB.deleteEventTypeofRank("111",receivers,store);

        /*for(ScoredStringL al : SUTB.TimeRanked){
            System.out.println(al.getString()+" , "+al.getScore());
        }*/
        System.out.println("final assert");

        // 333 because 222 uses the last five averages of times between inputs receiveds,
        // and those are only updated once an event is added ou removed.
        assertEquals("333",SUTB.findEventTypetoRellocate());

        store.Close();
        redisServer.stop();
    }
}

