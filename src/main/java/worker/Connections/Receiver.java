package worker.Connections;


public interface Receiver {

    void close(String argument);

    int eventsPerPeriod();

    long timeElapsed();

}
