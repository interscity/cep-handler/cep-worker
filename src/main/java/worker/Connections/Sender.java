package worker.Connections;

import org.apache.avro.generic.GenericData;

public interface Sender {

    void stopSending();

    void restartSending();

    int getEventsLost();

    void close(String type);

    void Publish(String routingKey, GenericData.Record event);

}
