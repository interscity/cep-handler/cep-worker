package worker.Connections;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.util.concurrent.TimeoutException;


import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import worker.Events.EventHandler;
import worker.utils.Serializer;

public class RabbitmqReceiver implements Receiver{
    private String queue_name;
    private static final String exchange_name = System.getenv("EXCHANGE");
    private static final String host = System.getenv("RABBITMQ_HOST");
    private static final String user = System.getenv("RABBITMQ_USERNAME");
    private static final String pass = System.getenv("RABBITMQ_PASSWORD");
    private long lastTimeUpdate;
    private Connection connection;
    private Channel channel;
    private final Consumer consumer;
    private int eventsReceivedperPeriod;
    //private static Logger LOG = LoggerFactory.getLogger(RabbitmqReceiver.class);
    private static final boolean logs_receiver = Boolean.parseBoolean(System.getenv("LOGS_RECEIVER"));


    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler,Connection c) {
        eventsReceivedperPeriod = 0;
        lastTimeUpdate = System.currentTimeMillis();
        try {
            //connection = c;
            channel = c.createChannel();
            if(logs_receiver) System.out.println("Receiver | new channel "+channel.getChannelNumber());
            channel.exchangeDeclare(exchange_name, "topic");
            if(logs_receiver) System.out.println("Receiver | declaring exchange : "+exchange_name);
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException e) {

            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                Record event = Serializer.AvroDeserialize(body, schema);
                eventHandler.handle(event, TypeName);
                long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler,Channel c) {
        lastTimeUpdate = System.currentTimeMillis();
        eventsReceivedperPeriod = 0;
        try {
            channel = c;
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                eventHandler.handle(Serializer.AvroDeserialize(body, schema), TypeName);
                //long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, TypeId+"t"+queue_name, consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema,String TypeId,String TypeName,final EventHandler eventHandler) {

        eventsReceivedperPeriod = 0;
        lastTimeUpdate = System.currentTimeMillis();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(user);
        factory.setPassword(pass);
        try {
            connection = factory.newConnection();
            if(logs_receiver) System.out.println("Receiver | new connection "+connection.getId()+" from factory ");
            channel = connection.createChannel();
            if(logs_receiver) System.out.println("Receiver | new channel "+channel.getChannelNumber());
            channel.exchangeDeclare(exchange_name, "topic");
            if(logs_receiver) System.out.println("Receiver | declaring exchange : "+exchange_name);
            queue_name = channel.queueDeclare().getQueue();
            if(logs_receiver) System.out.println("Receiver | new queue "+queue_name);
            channel.queueBind(queue_name, exchange_name, TypeId);
            if(logs_receiver) System.out.println("Receiver | binding queue to channel");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

        //if(channel.isOpen()) LOG.info("Channel open on creation\n");
        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                eventHandler.handle(Serializer.AvroDeserialize(body, schema), TypeName);
                //long deliveryTag = envelope.getDeliveryTag();
                //channel.basicAck(deliveryTag, false);
            }
        };

        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqReceiver(Schema schema, String xg,String TypeId, String TypeName, Channel channel, final EventHandler eventHandler) {
        //LOG.info("Creating Rabbitmq receiver connection");

        ;
        try {
            channel.exchangeDeclare(xg, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, xg, TypeId);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.channel = channel;
        //if(channel.isOpen()) LOG.info("Channel received on creation\n");

        consumer = new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                eventHandler.handle(Serializer.AvroDeserialize(body, schema), TypeName);
                eventsReceivedperPeriod++;
            }
        };
        try {
            channel.basicConsume(queue_name,true,"12344",false,true,null,consumer);
            //channel.basicConsume(queue_name, true, consumer);
        } catch (IOException | ShutdownSignalException e) {
            e.printStackTrace();
        }
    }


    private void CloseConnection()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
    private void CloseChannel()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void CloseQueue()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.queueDelete(queue_name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close(String type){
        if(type.equals("channel")) CloseQueue();
        if(type.equals("conn")) CloseChannel();
        if(type.equals("")) CloseConnection();
    }


    public int eventsPerPeriod(){
        //if(channel.isOpen()) System.out.print("Channel open for counting\n");
        int i = eventsReceivedperPeriod;
        eventsReceivedperPeriod = 0;
        return i;
    }
    public long timeElapsed(){
        long las = lastTimeUpdate;
        lastTimeUpdate = System.currentTimeMillis();
        return lastTimeUpdate-las;
    }

    void Reroute(Consumer consumer) {
        //if(channel.isOpen()) LOG.info("Channel open for rerouting\n");
        try {
            channel.basicConsume(queue_name, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        eventsReceivedperPeriod++;
    }


}
