package worker.DatabaseAccess;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.ScoredValue;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.api.sync.RedisSortedSetCommands;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.support.ConnectionPoolSupport;
import org.apache.avro.Schema;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Lettuce {

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> connection;
    private RedisCommands<String,String> syncCommands;
    private RedisSortedSetCommands<String,String> sortedSetCommands;
    //private Schema.Parser parser = new Schema.Parser();
    //private GenericObjectPool<StatefulRedisConnection<String, String>> pool;

    /*public Lettuce(){
        String host = System.getenv("REDIS_HOST");
        redisClient = RedisClient.create("redis://"+host+":6379");
        connection = redisClient.connect();
        syncCommands = connection.sync();
        sortedSetCommands = connection.sync();
    }*/

    public Lettuce(String hostname){
        String host;
        if( hostname == null) {
            host = System.getenv("REDIS_CEP_HOST");
            String pass = System.getenv("REDIS_CEP_PASS");
            RedisURI redisUri = RedisURI.Builder.redis(host)
                    .withPassword(pass)
                    .build();
            redisClient = RedisClient.create(redisUri);
        }
        else {
            host = hostname;
            redisClient = RedisClient.create("redis://" + host + ":6379");
        }

        //pool = ConnectionPoolSupport.createGenericObjectPool(() -> redisClient.connect(), new GenericObjectPoolConfig());

        connection = redisClient.connect();
        syncCommands = connection.sync();
        sortedSetCommands = connection.sync();
    }



    public String getEventTypeName(String id){
        return syncCommands.get(id+":Name");
       /* String result = null;
        System.out.println("inside m");
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(id+":Name");
            commands.exec();
            System.out.println("inside try, result is "+result);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("inside cath");
        }
        return result;

        */
    } // from cataloger

    public String getEventTypeDefinition(String id){
        /*String result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(id+":Definition");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;*/
        return syncCommands.get(id+":Definition");
    } // from cataloger

    public Schema getAvroSchema(String id){
        return new Schema.Parser().parse(syncCommands.get(id+":AvroSchema"));
        /*String result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(id+":AvroSchema");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Schema.Parser().parse(result);*/
    }

    public void setAvroSchema(String id,Schema schema){
        syncCommands.set(id+":AvroSchema",schema.toString());
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.set(id+":AvroSchema",schema.toString());
            //result = commands.get(id+":AvroSchema");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public String getEventTypeCurrentWorker(String uuid){
        return syncCommands.get(uuid+":WorkerId");
        /*String result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(uuid+":WorkerId");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void setEventTypeCurrentWorker(String uuid, String WorkerId){
        String previousWorkerId = syncCommands.get(uuid + ":WorkerId");
        syncCommands.set(uuid+":WorkerId",WorkerId);
        syncCommands.sadd("WorkerId"+WorkerId+"EventTypes",uuid);
        if(previousWorkerId!=null){
            syncCommands.srem("WorkerId"+previousWorkerId+"EventTypes",uuid);
        }
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            String previousWorkerId = commands.get(uuid + ":WorkerId");
            commands.set(uuid+":WorkerId",WorkerId);
            commands.sadd("WorkerId"+WorkerId+"EventTypes",uuid);
            if(previousWorkerId!=null){
                commands.srem("WorkerId"+previousWorkerId+"EventTypes",uuid);
            }
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }



    public Set<String> getEventTypeInputs(String id){   // from cataloger
        return syncCommands.smembers(id+":Inputs");
        /*Set<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.smembers(id+":Inputs");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void eventTypeStateBuildedAdd(String uuid, String WorkerId){
        syncCommands.sadd(uuid+":StateBuilded",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.sadd(uuid+":StateBuilded",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public boolean eventTypeStateBuilded(String uuid, String WorkerId){
        return syncCommands.sismember(uuid+":StateBuilded",WorkerId);
        /*boolean result = false;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.sismember(uuid+":StateBuilded",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void eventTypeStateBuildedDel(String uuid, String WorkerId){
        syncCommands.srem(uuid+":StateBuilded",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.srem(uuid+":StateBuilded",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void eventTypeAssigned(String uuid){
        //syncCommands.zpopmin("Unnasigned");
        //syncCommands.srem("Unnasigned",uuid);


        syncCommands.sadd("Assigned",uuid);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.sadd("Assigned",uuid);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public List<String> getUnnasignedEventTypes(){
        return syncCommands.zrange("Unnasigned",0,-1);
        /*List<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.zrange("Unnasigned",0,-1);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public String getUnnasignedEventType(){

        ScoredValue<String> result = syncCommands.zpopmin("Unnasigned");
        //System.out.println("Unn: "+result.getValue());


        /*ScoredValue<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.zpopmin("Unnasigned");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
        if(!result.hasValue())
            return null;
        else
            //uuid = result.getValue();
            //syncCommands.srem("RellocationPile",uuid);
            return result.getValue();

    }

    public boolean hasUnnasignedEvents(){
        return syncCommands.zcard("Unnasigned") > 0;
        /*boolean result = false;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.zcard("Unnasigned") > 0;
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void pushEventTypeForRellocation(String uuid,Double score){
        /////////////////syncCommands.sadd("RellocationPile",uuid);
        sortedSetCommands.zadd("RellocationStack",score,uuid);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.zadd("RellocationStack",score,uuid);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void RemoveEventTypeForRellocation(String uuid){
        /////////syncCommands.srem("RellocationPile",uuid);
        sortedSetCommands.zrem("RellocationStack",uuid);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.zrem("RellocationStack",uuid);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public boolean hasEventTypeBeenPickedUpForRellocation(String uuid, String WorkerId){
        return syncCommands.get(uuid + ":ReceveingWorkerId") != null;
        /*boolean result = false;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(uuid + ":ReceveingWorkerId") != null;
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
        
        //System.out.println("Receiving worker ID: "+syncCommands.get(uuid+":ReceveingWorkerId"));
        //System.out.println("Current worker id : "+WorkerId);
        //return !WorkerId.equals(syncCommands.get(uuid+":ReceveingWorkerId"));
    }

    public String popEventTypeForAcceptingRellocation(boolean softoverload,String WorkerId){

        ScoredValue<String> result = null;
        result = sortedSetCommands.zpopmax("RellocationStack");
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.zpopmax("RellocationStack");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */

        if (!result.hasValue()) return null;
        else if(getEventTypeCurrentWorker(result.getValue()).equals(WorkerId)){
            sortedSetCommands.zadd("RellocationStack",result.getScore(),result.getValue());
            return null;
        }
        else {
            if (softoverload) {
                sortedSetCommands.zadd("RellocationStack",result.getScore(),result.getValue());
                /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
                    RedisCommands<String, String> commands = connection.sync();
                    commands.multi();
                    commands.zadd("RellocationStack",result.getScore(),result.getValue());
                    commands.exec();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                 */
            }
            //uuid = result.getValue();
            //syncCommands.srem("RellocationPile",uuid);
            return result.getValue();
        }
    }

    public String getEventTypeReceiveingWorker(String uuid){
        return syncCommands.get(uuid+":ReceveingWorkerId");
        /*String result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(uuid+":ReceveingWorkerId");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void removeEventTypeReceveingWorker(String uuid){
        syncCommands.del(uuid+":ReceveingWorkerId");
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.del(uuid+":ReceveingWorkerId");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void setEventTypeReceveingWorker(String uuid, String WorkerId){
        syncCommands.set(uuid+":ReceveingWorkerId",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.set(uuid+":ReceveingWorkerId",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public boolean EventTypeCurrentWorker(String uuid, String WorkerId){
        return syncCommands.get(uuid+":WorkerId").equalsIgnoreCase(WorkerId);
        /*boolean result = false;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.get(uuid+":WorkerId").equalsIgnoreCase(WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public Set<String> getDeletedEventTypes(Set<String> currentEventTypes, String Workerid){
        Set<String> deletedIds = new HashSet<>();
        Set<String> RegisteredEventTypes = null;
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            RegisteredEventTypes = commands.smembers("registered");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
        RegisteredEventTypes = syncCommands.smembers("registered");

        if(RegisteredEventTypes!=null&&currentEventTypes!=null) {
            for (String TypeId : currentEventTypes) {
                if (!RegisteredEventTypes.contains(TypeId)) {
                    deletedIds.add(TypeId);
                }
            }
        }
        return deletedIds;
    }

    public Set<String> getAllEventTypesOfWorker(String WorkerId){
        return syncCommands.smembers("WorkerId"+WorkerId+"EventTypes");
        /*Set<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.smembers("registered");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public Set<String> getWorkerPods(){
        return syncCommands.smembers("Pods");
        /*Set<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.smembers("Pods");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void addWorkerPod(String WorkerId){
        if(syncCommands.sismember("RequestedWorkers",WorkerId))
            syncCommands.srem("RequestedWorkers",WorkerId);
        syncCommands.sadd("Workers",WorkerId);
        //syncCommands.sadd("PodCreationTime",String.valueOf(System.currentTimeMillis()));
        syncCommands.hset("PodsCreationTimestamp","worker"+WorkerId,String.valueOf(System.currentTimeMillis()));



        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            if(commands.sismember("RequestedWorkers",WorkerId))
                commands.srem("RequestedWorkers",WorkerId);
            commands.sadd("Workers",WorkerId);
            commands.sadd("PodCreationTime",String.valueOf(System.currentTimeMillis()));
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void addRequestedWorker(String WorkerId){
        syncCommands.sadd("RequestedWorkers",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.sadd("RequestedWorkers",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public String getAllWorkers(){
        String result = null;
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = "Requested Workers"+commands.smembers("RequestedWorkers");
            result = result+",Active Workers"+commands.smembers("Workers");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
        result = "Requested Workers"+syncCommands.smembers("RequestedWorkers");
        result = result+",Active Workers"+syncCommands.smembers("Workers");
        return result;
    }

    public Set<String> getEveryWorker(){
        Set<String> result = null;
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = "Requested Workers"+commands.smembers("RequestedWorkers");
            result = result+",Active Workers"+commands.smembers("Workers");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
        result = syncCommands.smembers("RequestedWorkers");
        result.addAll(syncCommands.smembers("Workers"));
        return result;
    }

    public int getTotalNumberOfWorkers(){
        int result = syncCommands.scard("Workers").intValue();
        result = result + syncCommands.scard("RequestedWorkers").intValue();
        /*int result = 0;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.scard("Workers").intValue();
            result = result+commands.scard("RequestedWorkers").intValue();
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
        return result;
    }

    public Set<String> getRequestedWorkers(){
        return syncCommands.smembers("RequestedWorkers");
    }

    public Set<String> getActiveWorkers(){
        return syncCommands.smembers("Workers");
    }

    public void setFullWorker(String WorkerId){
        syncCommands.sadd("FullWorkers",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.sadd("FullWorkers",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void remFullWorker(String WorkerId){
        syncCommands.srem("FullWorkers",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.srem("FullWorkers",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public int getNumberOfFullWorkers(){
        return syncCommands.scard("FullWorkers").intValue();
        /*int result = 0;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.scard("FullWorkers").intValue();
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }
    public Set<String> getFullWorkers(){
        return syncCommands.smembers("FullWorkers");
        /*Set<String> result = null;
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            result = commands.smembers("FullWorkers");
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

         */
    }

    public void removeFullworker(String WorkerId){
        syncCommands.srem("FullWorkers",WorkerId);
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.srem("FullWorkers",WorkerId);
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }


    public void setUnderloadWorker(String WorkerId,Double RA){
        //syncCommands.sadd("UnderloadWorkers",WorkerId);
        sortedSetCommands.zadd("UndWorkers",RA,WorkerId);
    }

    public void remUnderloadWorker(String WorkerId){
        syncCommands.zrem("UndWorkers",WorkerId);
    }

    public String getnextUnderloadWorkers(){
        ScoredValue<String> a = sortedSetCommands.zpopmin("UndWorkers");
        sortedSetCommands.zadd("UndWorkers",a.getScore(),a.getValue());
        return a.getValue();
    }

    public int getNumberOfUnderloadWorkers(){
        return Math.toIntExact(syncCommands.zcard("UndWorkers"));
    }



    public void setOverloadWorker(String WorkerId){
        syncCommands.sadd("HardOverloadWorkers",WorkerId);
    }

    public void remOverloadWorker(String WorkerId){
        syncCommands.srem("HardOverloadWorkers",WorkerId);
    }

    public Set<String> getOverloadWorkers(){
        return syncCommands.smembers("HardOverloadWorkers");
    }

    public int getNumberOfOverloadWorkers(){
        return Math.toIntExact(syncCommands.scard("HardOverloadWorkers"));
    }

    public void setNextUnderloadedWorkerToRemoval(String WorkerId){
        syncCommands.set("UnderLoadedWorkerToRemove",WorkerId);
    }

    public String getNextUnderloadedWorkerToRemoval(){
        return syncCommands.get("UnderLoadedWorkerToRemove");
    }

    public void deleteWorkerPod(String PodId){
        syncCommands.srem("Pods",PodId);
        syncCommands.sadd("PodExclusionTime",String.valueOf(System.currentTimeMillis()));
        /*try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> commands = connection.sync();
            commands.multi();
            commands.srem("Pods",PodId);
            commands.sadd("PodExclusionTime",String.valueOf(System.currentTimeMillis()));
            commands.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }

         */
    }

    public void Close(){
        connection.close();
        //pool.close();
        redisClient.shutdown();

    }

/*
    public void addToStateRank(String uuid,Integer score,Integer WorkerId){
        syncCommands.zadd("StateRank"+WorkerId,score,uuid);
    }

    public String getFromStateRank(Integer WorkerId){
        ScoredValue<String> resul = syncCommands.zpopmax("StateRank"+WorkerId);
        syncCommands.zadd("StateRank"+WorkerId,resul.getScore(),resul.getValue());
        return syncCommands.zrange("StateRank"+WorkerId,0,1).iterator().next();
    }

    public void removeFromStateRank(String uuid,Integer WorkerId){
        syncCommands.zrem("StateRank"+WorkerId,uuid);
    }

    public void deleteStateRank(Integer WorkerId){
        syncCommands.del("StateRank"+WorkerId);
    }

 */

}
