package worker.ClusterHandler;

import worker.DatabaseAccess.Lettuce;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Set;

public class KubernetesHandler implements ClusterHandler {

    private static boolean logs_k8s = Boolean.parseBoolean(System.getenv("LOGS_KUBERNETES"));

    private static boolean iow = Boolean.parseBoolean(System.getenv("INITIATE_OTHER_WORKERS"));

    private Random r = new Random(System.currentTimeMillis());


    public int instantiateNeoWorker(Lettuce l) {

        Set<String> pods = l.getWorkerPods();

        int value = r.nextInt(100000);
        while(pods.contains(value)){
            value = r.nextInt(100000);
        }


        /*int maxp = 0 ;
        for( String p : pods){
            if(Integer.parseInt(p)>maxp){
                maxp = Integer.parseInt(p);
            }
        }
        maxp++;

        value = maxp;
        */

        //l.addWorkerPod(String.valueOf(maxp));
        int result = createPod(String.valueOf(value));
        if(result!=0){
            l.addRequestedWorker(String.valueOf(value));
        }



        return result;
    }

    static private int createPod(String PodId){
        if(!iow){
            if (logs_k8s) System.out.println("K8 | Pod creation not Allowed");
            return 0;
        }
        else {
            if (logs_k8s) System.out.println("K8 | Creating new pod");
            return runExternalCommand("./main create " + PodId,Integer.parseInt(PodId));
        }

    }

    static private int runExternalCommand(String command,int uuid){
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);
        int result = 0;

        try {
            Process process = processBuilder.start();
            StringBuilder output = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                result = uuid;
                if(logs_k8s) System.out.println("K8 | go cmd executed");
                System.out.println(output);
            }
            else{
                if(logs_k8s) System.out.println("K8 | go cmd failed to execute");
                result = 0;

            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void StopAndRemoveWorkerFromCluster(String WorkerId, Lettuce L){
        //Lettuce L = new Lettuce(hostname);
        deletepod(WorkerId);
        L.deleteWorkerPod(WorkerId);
        //L.Close();
    }

    static private void deletepod(String PodId){
        if(logs_k8s) System.out.println("K8 | deleting pod"+PodId);
        runExternalCommand("./main delete "+PodId,Integer.parseInt(PodId));
    }
}
