package worker.Events;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.util.EventTypeBusModifier;
import com.espertech.esper.common.client.util.NameAccessModifier;
import com.espertech.esper.common.internal.event.avro.AvroSchemaEventType;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.*;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import worker.Connections.Sender;
import worker.Subscriber.Subscriber;

import java.lang.reflect.Array;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;


public class EventHandler {

    private static Logger LOG = LoggerFactory.getLogger(EventHandler.class);
    private EPRuntime epRuntime;
    private CompilerArguments arguments;
    private ConcurrentHashMap<String,Schema> schemas; //TypeId,schema
    private ConcurrentHashMap<Schema,Integer> schemasUsage;
    private ConcurrentSkipListSet<String> TypeNames;

    private static final boolean logs_ev = Boolean.parseBoolean(System.getenv("LOGS_EVENTHANDLER"));

    public EventHandler() {
        LOG.info("Initializing Event Handler Service ..");
        Configuration config = new Configuration();
        arguments = new CompilerArguments();
        config.getCommon().addEventTypeAutoName("worker");
        config.getCompiler().getByteCode().setAllowSubscriber(true);
        config.getCompiler().getByteCode().setAccessModifierEventType(NameAccessModifier.PUBLIC);
        config.getCompiler().getByteCode().setBusModifierEventType(EventTypeBusModifier.BUS);
        config.getCompiler().addPlugInSingleRowFunction("distance","worker.utils.Geo","distance");
        epRuntime = EPRuntimeProvider.getDefaultRuntime(config);
        arguments.setConfiguration(config);
        schemas = new ConcurrentHashMap<>();
        schemasUsage = new ConcurrentHashMap<>();
        TypeNames = new ConcurrentSkipListSet<>();

    }

    public Set<String> getEventTypeIds(){
        return schemas.keySet();
    }

    public void addInputStream(String TypeName,Schema scheme){
        if(!TypeNames.contains(TypeName)) {
            if(logs_ev) System.out.println("EV | New input stream : " + TypeName);
            TypeNames.add(TypeName);

            if(logs_ev) System.out.println("EV | Buiding inputStream ");
            String inputStream = "create avro schema " + TypeName + " ( ";
            //String[] sch = scheme.toString().split(",");
            //si = ;
            for (Object i : scheme.getFields().toArray()) {
                String[] si = i.toString().split(" ");
                inputStream = inputStream + si[0] + " ";
                inputStream = inputStream + si[1].split(":")[1].toLowerCase() + " , ";
            }
            inputStream = inputStream.substring(0, inputStream.length() - 3);
            inputStream = inputStream + " ) ";
            if(logs_ev) System.out.println("EV | Input Stream Built"+inputStream );

            EPCompiled compiled  = compile(inputStream, arguments);
            if(logs_ev) System.out.println("EV | Input Compiled");
            try {
                epRuntime.getDeploymentService().deploy(compiled, new DeploymentOptions().setDeploymentId(String.valueOf(TypeName)));
            } catch (EPDeployException e) {
                e.printStackTrace();
            }
            if(logs_ev) System.out.println("EV | Input Deployed");
        }
        else if(logs_ev) System.out.println("EV | input stream already registered: " + TypeName);
    }

    void deleteInputStream(String TypeName){
        TypeNames.remove(TypeName);
        try {
            epRuntime.getDeploymentService().undeploy(String.valueOf(TypeName));
        } catch (EPUndeployException e) {
            e.printStackTrace();
        }
    }

    public void removeInputs(Set<String> InputTypeNames){
        /*for(String typename : TypeNames){
            if(!currentTypeNames.contains(typename)){
                deleteInputStream(typename);
            }
        }

         */

        for(String inputName : InputTypeNames){
            deleteInputStream(inputName);
        }
    }


    private void addSchema(String TypeId, Schema scheme){
        schemas.put(TypeId,scheme);
        schemasUsage.putIfAbsent(scheme,0);
        schemasUsage.put(scheme,schemasUsage.get(scheme)+1);

    }

    public Schema getSchema(String TypeId){ return schemas.get(TypeId); }

    private void deleteSchema(String TypeId, Schema scheme){
        int currentvalue = schemasUsage.get(scheme);
        if(currentvalue >1)
            schemasUsage.put(scheme, currentvalue -1);
        else {
            schemasUsage.remove(scheme);
            schemas.remove(TypeId);
        }
    }

    public void addCheckExpression(String TypeId,String TypeName, String Query, Sender sender){
        if(logs_ev) System.out.print("EV | Adding event type for processing "+TypeName+"\n");
        arguments.getPath().add(epRuntime.getRuntimePath());
        if(logs_ev) System.out.println("EV | Adding epRuntime to arguments");

        //if(logs_ev) System.out.println("EV | Typename "+TypeName+" is "+epRuntime.getEventTypeService().getEventType());

        //if(logs_ev) System.out.println("EV | Compiling query");
        epRuntime.getRuntimeInstanceWideLock().writeLock().lock();
        Schema renamed;
        try {
            ////if(logs_ev) System.out.println("EV | Compiling query");
            ////EPCompiled compiled = compileQuery(Query,arguments);
            if(logs_ev) System.out.println("EV | Getting EPD");
            EPDeployment EPD = epRuntime.getDeploymentService().deploy(compileQuery(Query,arguments), new DeploymentOptions().setDeploymentId(TypeId));
            ////if(logs_ev) System.out.println("EV | Getting Schema");
            ////Schema sch = (Schema) ((AvroSchemaEventType) EPD.getStatements()[0].getEventType()).getSchema();
            if(logs_ev) System.out.println("EV | Renaming Schema");
            renamed = RenameSchema(TypeName, (Schema) ((AvroSchemaEventType) EPD.getStatements()[0].getEventType()).getSchema());
            if(logs_ev) System.out.println("EV | Seting subscriber");
            EPD.getStatements()[0].setSubscriber(new Subscriber(TypeId,sender, renamed));
            if(logs_ev) System.out.println("EV | Saving Schema");
            addSchema(TypeId, renamed);
        } catch (EPDeployException e) {
            e.printStackTrace();
        }
        finally {
            epRuntime.getRuntimeInstanceWideLock().writeLock().unlock();
        }

        if(logs_ev)System.out.print("EV | "+TypeName+" Added on EventHandler\n");
    }


    public void deleteCheckExpression(String TypeId) { /*refactor*/
        deleteSchema(TypeId,getSchema(TypeId));
        try {
            epRuntime.getDeploymentService().undeploy(TypeId);
        } catch (EPUndeployException e) {
            e.printStackTrace();
        }
    }

    private static EPCompiled compileQuery(String statement,CompilerArguments arguments){
        return compile("@EventRepresentation(avro) "+statement,arguments);
    }

    private static EPCompiled compile(String statement,CompilerArguments arguments){
        try {
            return EPCompilerProvider.getCompiler().compile(statement, arguments);
        } catch (EPCompileException e) {
            e.printStackTrace();
        }
        return null;
    }

    //private static Schema.Parser parser = new Schema.Parser();

    static Schema RenameSchema(String TypeName,Schema schema){
        //Schema.Parser parser = new Schema.Parser();
        String s = schema.toString();
        //System.out.print("s :"+s+"\n\n");

        String[] s2 = s.split(",");
        //System.out.print("s2[1] :"+s2[1]+"\n\n");
        s2[1] = "\"name\":\""+TypeName+"\"";
        //System.out.print("s2[1] :"+s2[1]+"\n\n");
        s = String.join(",", s2);
        //System.out.print("s :"+s+"\n\n");
        /*Schema renamed = parser.parse(s);
        if(null!=renamed){
            return renamed;
        }
        else{
            parser = new Schema.Parser();
            System.out.println("New Parser******************************************");
            return parser.parse(s);
        }

         */


        return new Schema.Parser().parse(s);
    }


    public void handle(Record Event,String TypeName) {
        epRuntime.getEventService().sendEventAvro(Event,TypeName);
    }



}
