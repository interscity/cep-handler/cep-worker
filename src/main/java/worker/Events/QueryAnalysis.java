package worker.Events;

public class QueryAnalysis {

    public static boolean hasDataWindow(String query){
        return query.contains("#");
    }

    public static String getStateType(String query){
        //System.out.println(query);
        //String[] r =  query.split("#");
        //System.out.println(r[1]);
        return query.split("#")[1].split("\\(")[0];
    }

    public static boolean IsItTimeBased(String query){
        String steype = query.split("#")[1].split("\\(")[0];
        switch(steype) {
            case "ext_timed":
                return true;
            case "ext_time_batch":
                return true;
            case "length":
                return false;
            case "length_batch":
                return false;
            default:
                return true;
        }


    }

    public static String getStateNumber(String query){
        String inter =  query.split("#")[1];
        //System.out.println("State number now : "+inter);
        inter = inter.split("\\(")[1];
        //System.out.println("State number now : "+inter);
        inter = inter.split("\\)")[0];
        //System.out.println("State number now : "+inter);
        return inter;
    }

    public static long analyzeQueryTime(String stateType,String stateNumber){
        if(stateType.equalsIgnoreCase("ext_timed")){
            //System.out.println("State number now : "+stateNumber.split(",")[1]);
            return Long.valueOf(stateNumber.split(",")[1]) * 1000;
        }
        else if(stateType.equalsIgnoreCase("ext_time_batch")){
            return 2 * Long.valueOf(stateNumber.split(",")[1]) * 1000;
        }
        else return 0;
    }

    public static int analyzeQueryLengh(String stateType,String stateNumber){
        if(stateType.equalsIgnoreCase("length")){
            //counters.put(Id, Integer.valueOf(stateNumber));
            return Integer.valueOf(stateNumber);
        }
        else if(stateType.equalsIgnoreCase("length_batch")) {
            //counters.put(Id, 2 * Integer.valueOf(stateNumber));
            return 2 * Integer.valueOf(stateNumber);
        }
        else return 0;
    }
}
