package worker.LoadBalancers;


import worker.Connections.Receiver;
import worker.DatabaseAccess.Lettuce;


import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;

public class InputSimilarity implements LoadBalancer{

    //private final ConcurrentSkipListMap<String,Set<String>> eventTypeIdToInputs;
    //private final String host;
    private static  boolean logs_lba = Boolean.parseBoolean(System.getenv("LOGS_LBA"));
    private final ConcurrentSkipListMap<String, Set<String>> invertedInputIndex;
    //private final ConcurrentSkipListMap<Integer, Set<String>> ScoreToEventTypeId;
    private final ConcurrentSkipListMap<Integer,StateUsageTimeBased> ScoreToEventTypesId;
    private final ConcurrentSkipListMap<String, Integer> EventTypeIdToScore;

    public InputSimilarity(){
        //eventTypeIdToInputs = new ConcurrentSkipListMap<>();
        //host = hostname;
        invertedInputIndex = new ConcurrentSkipListMap<>();
        //ScoreToEventTypeId = new ConcurrentSkipListMap<>();
        EventTypeIdToScore = new ConcurrentSkipListMap<>();
        ScoreToEventTypesId = new ConcurrentSkipListMap<>();
        //logs_lba = true;
    }


    public String findEventTypetoRellocate() { // finds the event type that uses the least used input
        if(logs_lba) print();
        return ScoreToEventTypesId.firstEntry().getValue().findEventTypetoRellocate();
    }

    private void print(){
        System.out.println("LBA-IS | number of ETS in LB: "+EventTypeIdToScore.size());
        //System.out.println("\nInputSimilarityRank:");
        for(Integer sco : ScoreToEventTypesId.keySet()) {
            System.out.println("\nScore "+sco);
            if (ScoreToEventTypesId.get(sco).size() == 0) System.out.println("\nScore "+sco+" is empty");
            else {
                for(double d : ScoreToEventTypesId.get(sco).allETs().keySet()){
                    System.out.println("Score : " + d + " , ids : " + ScoreToEventTypesId.get(sco).allETs().get(d).toString());
                }
                //for (ScoredStringD d : ScoreToEventTypesId.get(sco).allETs()) {
                //    System.out.println("Id : " + d.getString() + " , score : " + d.getScore());
                //}
            }
        }
    }

    public void addEventTypeToRank(String uuid, Map<String, Receiver> receiverMap,Lettuce store){
        Set<String> neoinputs = store.getEventTypeInputs(uuid);
        //if(logs_lba) System.out.println("add uuid"+uuid);
        updateRankplus(uuid,neoinputs,receiverMap,store);
    }

    void updateRankplus(String uuid,Set<String> neoinputs,Map<String, Receiver> receiverMap,Lettuce store){
        for(String inputId: neoinputs){
            if(invertedInputIndex.get(inputId)==null) {
                //if(logs_lba) System.out.println("Creating inverted for"+inputId);
                invertedInputIndex.put(inputId, new ConcurrentSkipListSet<>());
            }
            //if(logs_lba) System.out.println("adding ETs to inverted for"+inputId);
            invertedInputIndex.get(inputId).add(uuid);
            for(String TypeId : invertedInputIndex.get(inputId)){
                int usage = Integer.MAX_VALUE;
                //
                for(String InputType : store.getEventTypeInputs(TypeId)){
                    if(invertedInputIndex.get(InputType)==null){
                        //if(logs_lba) System.out.println("Creating inverted for"+InputType);
                        invertedInputIndex.put(InputType,new ConcurrentSkipListSet<>());
                    }
                    // if(logs_lba)System.out.println("Inverted size of"+InputType+" is "+invertedInputIndex.get(InputType).size());
                    if(invertedInputIndex.get(InputType).size()<usage)
                        usage = invertedInputIndex.get(InputType).size();
                }
                //if(logs_lba)System.out.println("Usage of"+TypeId+" is "+usage);
                if(EventTypeIdToScore.get(TypeId)!=null){
                    //if(logs_lba)System.out.println("Storing usage of"+TypeId);
                    int previousScore = EventTypeIdToScore.get(TypeId);
                    //if(logs_lba)System.out.println("previous score of"+TypeId+" is "+previousScore);
                    if(ScoreToEventTypesId.get(previousScore)!=null){
                        if (ScoreToEventTypesId.get(previousScore).size() == 1) {
                            ScoreToEventTypesId.remove(previousScore);
                            //if(logs_lba)System.out.println("Removing previous score from" + TypeId);
                        } else {
                            ScoreToEventTypesId.get(previousScore).deleteEventTypeofRank(TypeId, receiverMap, store);
                            //if(logs_lba)System.out.println("Removing previous score of" + TypeId);
                        }
                    }
                    //if(logs_lba)System.out.println("Storing usage of"+TypeId);
                }
                EventTypeIdToScore.put(TypeId,usage);
                if(ScoreToEventTypesId.get(usage)==null) {
                    ScoreToEventTypesId.put(usage, new StateUsageTimeBased(false));
                    //if(logs_lba)System.out.println("creating new score rank for"+usage);
                }
                ScoreToEventTypesId.get(usage).addEventTypeToRank(TypeId,receiverMap,store);
                //if(logs_lba)System.out.println("Storing Score to TypeId"+TypeId);
            }
        }
    }

    public void deleteEventTypeofRank(String uuid, Map<String, Receiver> receiverMap,Lettuce store){
        //if(logs_lba) System.out.println("removing uuid"+uuid);
        Set<String> messedInputs = store.getEventTypeInputs(uuid);
        for(String inputId : messedInputs) {
            if (invertedInputIndex.get(inputId).size() == 1) {
                invertedInputIndex.remove(inputId);
                //if(logs_lba) System.out.println("removing inputId from LBA"+inputId);
            }
            else {
                invertedInputIndex.get(inputId).remove(uuid);
                //if(logs_lba) System.out.println("decrementing inputId from LBA"+inputId);
            }
        }
        int previousScored = EventTypeIdToScore.get(uuid);
        //if(logs_lba)System.out.println("previous score of"+uuid+" is "+previousScored);
        if(ScoreToEventTypesId.get(previousScored).size()==1) {
            ScoreToEventTypesId.remove(previousScored);
            //if(logs_lba)System.out.println("removing "+previousScored+" from scorerank");
        }
        else{
            ScoreToEventTypesId.get(previousScored).deleteEventTypeofRank(uuid,receiverMap,store);
        }

        EventTypeIdToScore.remove(uuid);
        for(String inputId : messedInputs) {
            if(invertedInputIndex.get(inputId)!=null){
                for (String TypeId : invertedInputIndex.get(inputId)) {
                    int usage = Integer.MAX_VALUE;
                    for (String InputType : store.getEventTypeInputs(TypeId)) {
                        if (invertedInputIndex.get(InputType) == null) {
                            invertedInputIndex.put(InputType, new ConcurrentSkipListSet<>());
                        }
                        if(invertedInputIndex.get(InputType).size()<usage)
                            usage = invertedInputIndex.get(InputType).size();
                    }
                    if (EventTypeIdToScore.get(TypeId) == null) {
                        EventTypeIdToScore.put(TypeId, usage);

                        if(ScoreToEventTypesId.get(usage)==null)
                            ScoreToEventTypesId.put(usage,new StateUsageTimeBased(false));
                        ScoreToEventTypesId.get(usage).addEventTypeToRank(TypeId,receiverMap,store);

                    } else {
                        int previousScore = EventTypeIdToScore.get(TypeId);

                        if(ScoreToEventTypesId.get(previousScore).size()==1)
                            ScoreToEventTypesId.remove(previousScore);
                        else ScoreToEventTypesId.get(previousScore).deleteEventTypeofRank(TypeId,receiverMap,store);


                        if(ScoreToEventTypesId.get(usage)==null)
                            ScoreToEventTypesId.put(usage,new StateUsageTimeBased(false));
                        ScoreToEventTypesId.get(usage).addEventTypeToRank(TypeId,receiverMap,store);

                        EventTypeIdToScore.put(TypeId, usage);
                    }
                }
            }
        }
    }

}
