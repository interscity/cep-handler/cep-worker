package worker.LoadBalancers;


import worker.Connections.Receiver;
import worker.DatabaseAccess.Lettuce;

import java.util.Map;


public interface LoadBalancer {

    String findEventTypetoRellocate();

    void addEventTypeToRank(String uuid, Map<String, Receiver> receiverMap, Lettuce store);

    void deleteEventTypeofRank(String uuid, Map<String, Receiver> receiverMap,Lettuce store);

}
