package worker.Subscriber;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import worker.Connections.Sender;

import java.util.Map;

public class Subscriber {
    private String typeId;
    private Sender sender;
    private Schema schema;
    private Record event;
    private boolean subfield;
    private Record eventT;
    private String firstKey;

    /* The event type is also used as the Routing Key */

    public Subscriber(String TypeId, Sender sender, Schema scheme){
        this.sender = sender;
        this.schema = scheme;
        this.typeId = TypeId;
    }

    public void update(Map row) {
        event = fromMap(row,schema);
        //System.out.print(event.getSchema().getName()+" :-- "+"\n");
        sender.Publish(typeId, event);
    }

    private Record fromMap(Map row, Schema schema){
        eventT = new Record(schema);
        subfield = true;
        firstKey = (String) row.keySet().toArray()[0];
        for(Schema.Field field : schema.getFields()){
            if(field.name().equals(firstKey)){
                subfield = false;
                for (Object i : row.keySet()) {
                    eventT.put((String.valueOf(i)), row.get(i));
                }
                break;
            }
        }

        if(subfield){
            eventT = (Record) row.get(firstKey);
        }
        return eventT;
    }

}