package worker.utils;
import static java.lang.Math.*;

public class Geo {
    private static final double EARTH_RADIUS = 6368;

    public static double distance( double lat1, double lon1, double lat2, double lon2){
        // in Kilometers
        return 2 * EARTH_RADIUS * asin(sqrt(pow(sin((lat1-lat2)*PI/360),2)+cos(lat1*PI/180)*cos(lat2*PI/180)*pow(sin((lon1-lon2)*PI/360),2)));

    }

}
